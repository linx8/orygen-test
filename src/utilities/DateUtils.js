import moment from "moment";

// returns current timestamp using moment
const getTimestamp = () => {
  return moment().format();
};

// returns display timestamp in format
const getDisplayTimestamp = timestamp => {
  const defaultFormat = "DD MMM, hh.mma";

  const displayTime = moment(timestamp);

  return displayTime.format(defaultFormat);
};

// Get relative time from moment js
const getRelativeTime = timestamp => {
  return moment(timestamp).fromNow();
};

export { getTimestamp, getRelativeTime, getDisplayTimestamp };
