import React, { useState } from "react";
import PostForm from "../components/PostForm";
import PostFeed from "../components/PostFeed";
import mockFeed from "../data/mockFeed";

/*
 * This is a 'dumb' view for the feed written as a simple function
 * It renders the view and markup, and handles the form submit
 * Has a mention variable
 */
const PublicFeed = () => {
  // initialises with mock feed data
  const [feed, addToFeed] = useState(mockFeed);
  const [mention, addMention] = useState("");

  const onSubmit = data => {
    const newFeed = feed.slice(0);
    newFeed.unshift(data);
    addToFeed(newFeed);
    addMention(""); // clear the mention after we post
  };

  // we handle the react at this level and pass down to child
  const handleReact = name => {
    addMention(name);
  };

  return (
    <div>
      <h1 className="u-text-center">Public newsfeed post</h1>
      <PostForm isPublic onSubmit={onSubmit} mention={mention} />
      <PostFeed isPublic feed={feed} handleReact={handleReact} />
    </div>
  );
};

export default PublicFeed;
