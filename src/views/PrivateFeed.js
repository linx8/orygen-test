import React, { useState } from "react";
import PostForm from "../components/PostForm";
import PostFeed from "../components/PostFeed";
import mockFeed from "../data/mockFeed";

/*
 * This is a 'dumb' view for the feed written as a simple function
 * It renders the view and markup, and handles the form submit
 */
const PrivateFeed = () => {
  // initialises with mock feed data
  const [feed, addToFeed] = useState(mockFeed);

  const onSubmit = data => {
    // do something with the data
    const newFeed = feed.slice(0);
    newFeed.unshift(data);
    addToFeed(newFeed);
  };

  return (
    <div>
      <h1 className="u-text-center">Private clinical log post</h1>
      <PostForm public={false} onSubmit={onSubmit} />
      <PostFeed feed={feed} />
    </div>
  );
};

export default PrivateFeed;
