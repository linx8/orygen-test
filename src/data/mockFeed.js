const mockFeed = [
  {
    name: "Vince",
    post: "This is a test",
    timestamp: "2020-06-13T12:02:52+10:00"
  },
  {
    name: "Vicky",
    post: "This is also a test",
    timestamp: "2020-06-11T01:36:49.321Z",
    isVenting: true
  }
];

export default mockFeed;
