import React from "react";
import { getRelativeTime, getDisplayTimestamp } from "../utilities/DateUtils";
import checkForMentions from "../utilities/StringUtils";

/*
 * This component is a single post used in the feed
 */
const Post = ({ name, timestamp, isVenting, post, handleReact, isPublic }) => {
  // The react function is passed down as a prop, and is fired with the name of the post
  const onReact = () => {
    handleReact(name);
  };

  // get unique mentions
  const mentions = checkForMentions(post);

  const displayTimestamp = isPublic
    ? getRelativeTime(timestamp)
    : getDisplayTimestamp(timestamp);
  return (
    <div className="c-post">
      <div className="c-post__header">
        <div className="c-post__header-left">
          <span className="c-post__image" />
          <span>{name}</span>
        </div>
        <div className="c-post__meta">
          <div className="c-post__timestamp">{displayTimestamp}</div>
          {isPublic && <div>{isVenting && `I'm just venting`}</div>}
        </div>
      </div>
      <div className="c-post__content">{post}</div>
      <div className="u-flex-center">
        <div className="u-flex-1">
          {isPublic && mentions && (
            <span className="c-post__mentions">
              Mentions: <span className="u-bold">{mentions.join(", ")}</span>
            </span>
          )}
        </div>

        {isPublic && (
          <div className="u-text-right">
            <button className="c-post__button" type="button" onClick={onReact}>
              React
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Post;
