import React, { useState, useEffect, useRef } from "react";
import { getTimestamp } from "../utilities/DateUtils";

/*
 * The post form component, handles form submission and fields
 * @params onSubmit: Pass through a function for submitting, and call from here
 * @params isPublic: flag for changing behavior
 * @params mention: mention from the react button added to the form
 */

function PostForm({ onSubmit, isPublic, mention }) {
  // this creates a variable in the state called post using react hooks without classes,
  const [post, setPost] = useState("");
  const [name, setName] = useState("Admin"); // default name
  const [isVenting, setVenting] = useState(false);

  // create a reference object for this component to hold the previous mention
  const prevMention = useRef();

  /* useEffect runs every time mention or post changes.
   * We only update the post to include unique mentions, once
   */
  useEffect(() => {
    // checks if theres a mention and that the mention has changed from previous
    if (mention && mention !== prevMention.current) {
      setPost(`${post}@${mention} `);
      window.scrollTo(0, 0);
    }
    // we save the mention passed in from prop
    prevMention.current = mention;
  }, [mention, post]);

  const handleSubmit = e => {
    e.preventDefault();
    const timestamp = getTimestamp();
    onSubmit({ post, isVenting, timestamp, name });
    clearFields();
  };

  const clearFields = () => {
    setPost("");
    setVenting(false);
    prevMention.current = "";
  };
  return (
    <form className="c-form" onSubmit={handleSubmit}>
      <ul className="c-form__fields">
        <li>
          <input
            type="hidden"
            name="text-name"
            value="Test Name"
            onChange={e => setName(e.target.value)}
          />
        </li>
        <li>
          <textarea
            name="text-post"
            type="textarea"
            value={post}
            onChange={e => setPost(e.target.value)}
            placeholder={
              isPublic &&
              "Click on react to reply to a post. Or type @name manually"
            }
          />
        </li>
        {isPublic && (
          <li>
            <label>
              <input
                type="checkbox"
                name="chkbox-venting"
                checked={isVenting}
                value={isVenting}
                onChange={e => setVenting(!isVenting)}
              />
              I'm just venting
            </label>
          </li>
        )}
      </ul>
      <button
        disabled={!post && "disabled"}
        className="c-form__submit"
        type="submit"
      >
        Post
      </button>
    </form>
  );
}

export default PostForm;
