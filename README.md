# Orygen Test - Newsfeed posts

Contains two views, a public newsfeed and a private clinical log.

## Public newsfeed features:

- Can @mention users by their name. Supports multiple mentions
- Can react to posts which will add @mention to your post.
- Shows relative timestamp
- Has venting checkbox

## Private Clinical log

- Supports a feed of posts
- Has absolute timestamps

Tested on:

- Chrome
- Firefox
- Safari

- Mobile simulator

Created bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Tech stack:

- React ~v16
- React Router (for routing)
- Scss (for styling)
- Moment js for time utilities

## Setup

In the project directory, run command:
`npm install` or `yarn`

To start the app, run:
`npm start` or `yarn start`

This will start the app on http://localhost:3000

# Build

You can also build the app using `npm run build` or `yarn build`

## Learn More about Create React App below

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
