import React from "react";
import { Link } from "react-router-dom";

const Layout = ({ children }) => (
  <div className="app">
    <header className="c-header">
      <nav className="c-nav">
        <div className="o-container">
          <ul className="c-nav__menu">
            <li className="c-nav__menu-home">
              <Link to="/">O</Link>
            </li>
            <li>
              <Link to="/">Public Newsfeed</Link>
            </li>
            <li>
              <Link to="/private-feed">Private Clinical Log</Link>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <div className="o-content">
      <div className="o-container">{children}</div>
    </div>
  </div>
);

export default Layout;
