/*
 * This function checks a string for any @words
 * Supports multiple mentions and distinct mentions
 * Params: post: string
 * returns: array of @words
 */
const checkForMentions = post => {
  if (post.indexOf("@") > 0) {
    // regex is any word starting with @
    const matches = [...post.matchAll(/(@)\w+/g)];

    // a Set will only hold unique values
    return [...new Set(matches.map(m => m[0]))]; // we only care about the match string
  }
  return null;
};

export default checkForMentions;
