import moment from "moment";

// This function sorts the posts by timestamp
const sortByTimestamp = posts => {
  const sortedArray = posts.sort((a, b) =>
    moment(a.timestamp).diff(moment(b.timestamp))
  );
  return sortedArray;
};

export default sortByTimestamp;
