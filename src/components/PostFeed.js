import React from "react";
import Post from "./Post";

// a post feed contains multiple posts
const PostFeed = ({ feed, isPublic, handleReact }) => {
  return (
    feed && (
      <div className="c-feed">
        {feed.map(post => {
          return (
            // a key is a unique ID for each post used by React for rerendering
            <Post
              key={`${post.name}-${post.timestamp}`}
              {...post}
              isPublic={isPublic}
              handleReact={handleReact}
            />
          );
        })}
      </div>
    )
  );
};

export default PostFeed;
