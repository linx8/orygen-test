import React from "react";
import Layout from "./views/Layout";
import PublicFeed from "./views/PublicFeed";
import PrivateFeed from "./views/PrivateFeed";

import "./scss/app.scss";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path="/private-feed">
            <PrivateFeed />
          </Route>
          <Route path="/public-feed">
            <PublicFeed />
          </Route>
          <Route path="/">
            <PublicFeed />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
